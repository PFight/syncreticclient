import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routes } from './app.routes';

import { HomeModule } from './home/home.module';
import { SharedModule } from './shared/shared.module';

import * as Tech500 from './external/angular2-tree-component-master/angular2-tree-component';

@NgModule({
  imports: [BrowserModule, HttpModule, RouterModule.forRoot(routes), 
    HomeModule, SharedModule.forRoot(), Tech500.TreeModule],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  }],
  bootstrap: [AppComponent]

})

export class AppModule { }
