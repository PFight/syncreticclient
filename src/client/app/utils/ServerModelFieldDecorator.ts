
/// <reference path="../../../../typings/globals/reflect-metadata/index.d.ts" />

/**
 * Decorator, that saves appropriate server model name for the property.
 * @see convertToServer, convertFromServer
 * @param name Name of the field in the server model.
 * @param type Type of the field (if the field is complex object).
 * @example
 *    @ServerModelField("doc", ScorocodeUserInfo)
 *     mDoc: ScorocodeUserInfo;
 */
export function ServerModelField<T>(name?: string) {
    return (target: Object, propertyKey: string) => {
        Reflect.defineMetadata(ServerNameMetadataKey, name || propertyKey, target, propertyKey);

        var availableNames = Reflect.getMetadata(AvailableFieldsMetadataKey, target);
        if (!availableNames) {
            availableNames = [];
            Reflect.defineMetadata(AvailableFieldsMetadataKey, availableNames, target);            
        }
        availableNames.push(propertyKey);
    }
}

/**
 * Convert object to the server-friendly json, usign @ServerModelField decorators info.
 * @param clientObj Instance of the class, that uses @ServerModelField decorators on it's fields.
 */
export function serialize<T>(clientObj: T): string {
    return JSON.stringify(convertToServer<T>(clientObj));
}

/**
 * Convert object to the server-friendly, using @ServerModelField decorators info.
 * @param clientObj Instance of the class, that uses @ServerModelField decorators on it's fields.
 */
export function convertToServer<T>(clientObj: T): Object {
    var serverObj = {};

    var target = Object.getPrototypeOf(clientObj);
    var availableNames = Reflect.getMetadata(AvailableFieldsMetadataKey, target) as [string];
    availableNames.forEach(propKey=> {        
        var serverName = Reflect.getMetadata(ServerNameMetadataKey, target, propKey);
        if (serverName) {
            var clientVal = clientObj[propKey];
            var serverVal = clientVal;
            if (clientVal && typeof(clientVal) == "object") {
                serverVal = convertToServer(clientVal);
            }
            serverObj[serverName] = serverVal;
        }
    });

    if (!availableNames) {
        errorNoPropertiesFound(parseTypeName(clientObj.constructor.toString()));
    }

    return serverObj;
}

/**
 * Convert json, reterived from a server to client object using @ServerModelField decorators info.
 * @param serverObj Json data from the server.
 * @param clientObj Empty object, that will be result of the conversion.
 */
export function parse<T>(serverJson: string, type: { new(): T ;} ): T {
    return convertFromServer<T>(JSON.parse(serverJson), type);
}

/**
 * Convert object, reterived from a server to client object using @ServerModelField decorators info.
 * @param serverObj Parsed json data from the server.
 * @param clientObj Empty object, that will be result of the conversion.
 */
export function convertFromServer<T>(serverObj: Object, type: { new(): T ;} ): T {
    var clientObj: T = new type();
    var target = Object.getPrototypeOf(clientObj);
    var availableNames = Reflect.getMetadata(AvailableFieldsMetadataKey, target) as [string];
    if (availableNames) {
        availableNames.forEach(propKey => {
            var serverName = Reflect.getMetadata(ServerNameMetadataKey, target, propKey);
            if (serverName) {     
                var serverVal = serverObj[serverName];
                var clientVal = clientObj[propKey];
                var propType = Reflect.getMetadata("design:type", target, propKey); // Reflect.getMetadata(ActivatorMetadataKey, target, propKey);
                var propTypeServerFields =  Reflect.getMetadata(AvailableFieldsMetadataKey, propType.prototype) as [string];
                var propTypeHasServerFields = propTypeServerFields && propTypeServerFields.length > 0;
                if (propTypeHasServerFields) {
                    clientVal = convertFromServer(serverVal, propType);
                } else {
                    clientVal = serverVal;
                }
                if (!propTypeHasServerFields && typeof(clientVal) == "object") {
                    console.warn("Second parameter of @ServerModelField directive for property '" + propKey + 
                        "' in type '" + getTypeName(type) + "' is not specified. Object will not be converted.");
                }
                clientObj[propKey] = clientVal;
            }
        });
    } else {
        errorNoPropertiesFound(getTypeName(type));
    }

    return clientObj;
}

// Helpers ----------------------------------------------------------------------------

const AvailableFieldsMetadataKey = "Syncretic_AvailableFields";
const ServerNameMetadataKey = "Syncretic_ServerName";

function errorNoPropertiesFound<T>(typeName: string) {
    throw new Error("There is no @ServerModelField directives in type '" + typeName + "'. Nothing to convert.");
}

function getTypeName<T>(type: { new(): T ;}) {
     return parseTypeName(type.toString());
}

function parseTypeName(ctorStr: string) {
     var matches = ctorStr.match(/\w+/g);
     if (matches.length > 1) {
         return matches[1];
     } else {
         return "<can't determine type name>";
     
    }
}