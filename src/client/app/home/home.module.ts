import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { NameListService } from '../shared/name-list/index';
import * as Tech500 from '../external/angular2-tree-component-master/angular2-tree-component';


@NgModule({
  imports: [CommonModule, SharedModule, Tech500.TreeModule],
  declarations: [HomeComponent],
  exports: [HomeComponent],
  providers: [NameListService]
})
export class HomeModule { }
