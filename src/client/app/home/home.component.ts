import { Component, OnInit, ApplicationRef , ViewChild, ChangeDetectorRef } from '@angular/core';
import { NameListService } from '../shared/index';

import * as Tech500 from '../external/angular2-tree-component-master/angular2-tree-component';
import {ScorocodeUser } from "../data/scorocode/access/ScorocodeUser";

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit {

  newName: string = '';
  errorMessage: string;
  names: any[];
  obj: any;

  @ViewChild('modelTree') modelTree: Tech500.TreeComponent;
  
  /**
   * Creates an instance of the HomeComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  constructor(public nameListService: NameListService, private appRef: ApplicationRef, private cd: ChangeDetectorRef ) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getNames();
    this.obj = { hoi: "punks" };

    var data = { username: "PFight", email: "PFIght77@gmail.com", doc: { info: "hello user" } };
    var user = ScorocodeUser.parse(data);
    console.info("data: ", data);
    console.info("user: ", user);
    var newData = user.serialize();
    console.info("newData: ", newData);

  }

  /**
   * Handle the nameListService observable
   */
  getNames() {
    this.nameListService.get()
		     .subscribe(
		       names => this.names = this.loadTree(names),
		       error =>  this.errorMessage = <any>error
		       );
  }

  private loadTree(names: string[]): any[] {
    return names.map(x => { return {name: x}; });
  }

  /**
   * Pushes a new name onto the names array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  addName(): boolean {
    // TODO: implement nameListService.post
    // this.names = JSON.parse(JSON.stringify(this.names));
    this.names.push({name: this.newName});
    this.newName = '';
    this.cd.markForCheck();
    
    console.info('tick');
    this.modelTree.updateTree();
    this.appRef.tick();
    return false;
  }

  public updateNodes() {
    this.modelTree.ngOnChanges({
      options: { currentValue: this.modelTree.options },
      nodes: { currentValue: this.modelTree.nodes }
    });
  }

}
