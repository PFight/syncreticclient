import {ILoadQuery, IEntityInfo} from "./Queries";
import {IDataEntity} from "../data/IDataEntity";
import { Observable } from 'rxjs';
import { OperationResult, IOperationResult } from "./OperationResult";
import {ITimestampObject} from "../data/ITimestampObject";

export interface ISyncreticDataProvider {
    load(query: ILoadQuery): Observable<IDataEntity[]>;
    loadAll(query: ILoadQuery[]): Observable<IDataEntity[]>;
    loadSingle(query: ILoadQuery): Observable<IDataEntity>;

    save(entity: IDataEntity): Observable<IOperationResult>;
    saveAll(entity: IDataEntity[]): Observable<IOperationResult>;

    add(entity: IDataEntity): Observable<IOperationResult>;
    addAll(entity: IDataEntity[]): Observable<IOperationResult>;

    delete(entity: IDataEntity): Observable<IOperationResult>;
    deleteAll(entity: IDataEntity[]): Observable<IOperationResult>;
    
    loadTimestamp(target: IEntityInfo): Observable<ITimestampObject>;
    loadTimestampAll(targets: IEntityInfo[]): Observable<ITimestampObject[]>;
}