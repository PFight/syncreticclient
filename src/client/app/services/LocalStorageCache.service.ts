import { Injectable } from '@angular/core';
import { ISyncreticDataProvider } from "./ISyncreticDataProvider";
import { ILoadQuery, IEntityInfo} from "./Queries";
import { IDataEntity} from "../data/IDataEntity";
import { Observable } from 'rxjs';
import { OperationResult, IOperationResult } from "./OperationResult";
import { ITimestampObject} from "../data/ITimestampObject";
import { BackendService } from "./BackendService";

@Injectable()
export class LocalStorageCacheService implements ISyncreticDataProvider {

    LocalStorageCacheService(private backend: BackendService) {

    }

    load(query: ILoadQuery): Observable<IDataEntity[]> {
        return backend.load(query);
    }
    loadAll(query: ILoadQuery[]): Observable<IDataEntity[]> {
        return backend.loadAll(query);
    }
    loadSingle(query: ILoadQuery): Observable<IDataEntity> {
        return backend.loadSingle(query);
    }

    save(entity: IDataEntity): Observable<IOperationResult> {
        return backend.save(entity);
    }
    saveAll(entity: IDataEntity[]): Observable<IOperationResult> {
        return backend.saveAll(entity);
    }

    add(entity: IDataEntity): Observable<IOperationResult> {
        return backend.add(entity);
    }
    addAll(entity: IDataEntity[]): Observable<IOperationResult> {
        return backend.addAll(entity);
    }

    delete(entity: IDataEntity): Observable<IOperationResult> {
        return backend.delete(entity);
    }
    deleteAll(entity: IDataEntity[]): Observable<IOperationResult> {
        return backend.deleteAll(entity);
    }
    
    loadTimestamp(target: IEntityInfo): Observable<ITimestampObject> {
        return backend.loadTimestamp(targetID);
    }
    loadTimestampAll(targets: IEntityInfo[]): Observable<ITimestampObject[]> {
        return backend.loadTimestampAll(targetIDs);
    }
}