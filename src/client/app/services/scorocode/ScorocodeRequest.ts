export interface IScorocodeRequest {
    /** идентификатор приложения, обязательный */
    app: string;
    /** клиентский ключ, обязательный */
    cli: string;
}

export interface IScorocodeResponse {
    error?: boolean;
    /** 4XX/5XX,  Код ошибки */
    errCode?: string;
    /** Текст ошибки */
    errMsg?: string;
    result?: Object;
}


export interface IDataScocorocdeRequest extends IScorocodeRequest {
    /** ключ доступа, необязательный, для полного доступа masterKey */
    acc: string;
    /** ID сессии, обязательный, если ACLPublic приложения на операцию == false и acc != masterKey */
    sess?: string;
    /**  имя коллекции, обязательный */
    coll: string;
}

export interface IDataInsertScorocodeRequest extends IDataScocorocdeRequest {
    /** документ с парами имя_поля:значение, необязательный */
    doc?: Object;
}

export interface IDataRemoveScorocodeRequest extends IDataScocorocdeRequest {
    /** запрос с парами имя_поля/оператор:значение, необязательный */
    query?: string;
    /** лимит количества удаляемых документов, необязательный, если не указан, то удалятся первые 1000 документов */
    limit?: number;
}

export interface IDataUpdateScorocodeRequest extends IDataScocorocdeRequest {
    /** запрос с парами имя_поля/оператор:значение, необязательный */
    query?: string;
    /** документ с парами оператор:значение, обязательный */
    doc: Object;
    /** лимит количества обновляемых документов, необязательный, если не указан, то обновятся первые 1000 документов */
    limit?: number;
}

export interface IDataUpdateByIDScorocodeRequest extends IDataScocorocdeRequest {
    /** запрос в формате "_id" : "<идентификатор документа>", обязательный */
    query: string;
    /** документ с парами оператор:значение, обязательный */
    doc: Object;
}

export interface IDataFindScorocodeRequest extends IDataScocorocdeRequest {
    /** запрос с парами имя_поля/оператор:значение, необязательный, если пустой, то будут возвращены первые 100 документов */
    query?: string;
    /** сортировка по полям в формате имя_поля:1/-1, необязательный */
    sort?: Object;
    /** список имен полей, которые будут возвращены в каждом документе, необязательный */
    fields?: string[];
    /** лимит размера выборки, необязательный, по умолчанию 50 */
    limit?: number;
    /** количество документов, которое нужно пропустить в выборке */
    skip?: number;
}

export interface IDataCountScorocodeRequest extends IDataScocorocdeRequest {
    /** запрос с парами имя_поля/оператор:значение, необязательный */
    query: string;
}
