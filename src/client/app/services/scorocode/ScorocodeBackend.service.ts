import { Injectable } from '@angular/core';
import { ISyncreticDataProvider } from "../ISyncreticDataProvider";
import { ILoadQuery, IEntityInfo} from "../Queries";
import { IDataEntity} from "../../data/IDataEntity";
import { Observable } from 'rxjs';
import { OperationResult, IOperationResult } from "../OperationResult";
import { ITimestampObject} from "../../data/ITimestampObject";
import { BackendService } from "../BackendService";

@Injectable()
export class ScorocodeBackendService extends BackendService {

    ScorocodeBackendService() {

    }

    load(query: ILoadQuery): Observable<IDataEntity[]> {
        return null;
    }
    loadAll(query: ILoadQuery[]): Observable<IDataEntity[]> {
        return null;
    }
    loadSingle(query: ILoadQuery): Observable<IDataEntity> {
        return null;
    }

    save(entity: IDataEntity): Observable<IOperationResult> {
        return null;
    }
    saveAll(entity: IDataEntity[]): Observable<IOperationResult> {
        return null;
    }

    add(entity: IDataEntity): Observable<IOperationResult> {
        return null;
    }
    addAll(entity: IDataEntity[]): Observable<IOperationResult> {
        return null;
    }

    delete(entity: IDataEntity): Observable<IOperationResult> {
        return null;
    }
    deleteAll(entity: IDataEntity[]): Observable<IOperationResult> {
        return null;
    }
    
    loadTimestamp(target: IEntityInfo): Observable<ITimestampObject> {
        return null;
    }
    loadTimestampAll(targets: IEntityInfo[]): Observable<ITimestampObject[]> {
        return null;
    }
}