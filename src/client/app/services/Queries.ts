import { ModelCategoryTree, ModelCategoryTreeTimestamp } from "../data/category-tree/ModelCategoryTree";
import { SyncreticModel } from "../data/syncretic-model/SyncreticModel";
import {ModelElement} from "../data/syncretic-model/ModelElement";
import {ModelCategory} from "../data/category-tree/ModelCategory";

export enum LoadQueryType {
    LoadCategoryTree,
    LoadSyncreticModel,
    LoadComments,
    LoadHistory,
    LoadObjects
}

export interface IEntityInfo {
    ID: string;    
    type: EntityType;
    timestamp?: ITimestampObject;
}

export interface ILoadQuery {
    queryType: LoadQueryType;
}

export class CategoryTreeQuery implements ILoadQuery {
    public get queryType() { return LoadQueryType.LoadCategoryTree; }
    public entityInfo: IEntityInfo;
}

export class SyncreticModelQuery implements ILoadQuery {
    public get queryType() { return LoadQueryType.LoadSyncreticModel; }
    public entityInfo: IEntityInfo;
    public category: ModelCategory;
}

export class ObjectsQuery implements ILoadQuery {
    public get queryType() { return LoadQueryType.LoadObjects; }
    public elements: ModelElement[];
}

export class CommentsQuery implements ILoadQuery {
    public get queryType() { return LoadQueryType.LoadComments; }
    public entityInfo: IEntityInfo;
}

export class HistoryQuery implements ILoadQuery {
    public get queryType() { return LoadQueryType.LoadHistory; }
    public entityInfo: IEntityInfo;
}

