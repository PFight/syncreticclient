import { Injectable } from '@angular/core';
import { ISyncreticDataProvider } from "./ISyncreticDataProvider";
import { ILoadQuery, IEntityInfo} from "./Queries";
import { IDataEntity} from "../data/IDataEntity";
import { Observable } from 'rxjs';
import { OperationResult, IOperationResult } from "./OperationResult";
import { ITimestampObject} from "../data/ITimestampObject";

@Injectable()
export abstract  class BackendService implements ISyncreticDataProvider {
    abstract load(query: ILoadQuery): Observable<IDataEntity[]>;
    abstract loadAll(query: ILoadQuery[]): Observable<IDataEntity[]>;
    abstract loadSingle(query: ILoadQuery): Observable<IDataEntity>;

    abstract save(entity: IDataEntity): Observable<IOperationResult>;
    abstract saveAll(entity: IDataEntity[]): Observable<IOperationResult>;

    abstract add(entity: IDataEntity): Observable<IOperationResult>;
    abstract addAll(entity: IDataEntity[]): Observable<IOperationResult>;

    abstract delete(entity: IDataEntity): Observable<IOperationResult>;
    abstract deleteAll(entity: IDataEntity[]): Observable<IOperationResult>;
    
    abstract loadTimestamp(target: IEntityInfo): Observable<ITimestampObject>;
    abstract loadTimestampAll(targets: IEntityInfo[]): Observable<ITimestampObject[]>;
}