import {IUserRole} from "../../access/IUserRole";

export class ScorocodeUserRole implements IUserRole {
    mName: string;
    
    public get displayName(): string {
        return this.mName;
    }
    public set displayName(val: string) {
        this.mName = val;
    }

    
    public get ID(): string {
        return this.mName;
    }

    public set ID(val: string) {
        this.mName = val;
    }

    public serialize(): Object {
        return this.mName;
    }

    public static parse(data: Object): ScorocodeUserRole {
        var role = new ScorocodeUserRole();
        role.mName = data as string;
        return role;
    }
}