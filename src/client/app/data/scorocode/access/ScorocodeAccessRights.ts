import {IUser} from "../../access/IUser";
import {IUserRole} from "../../access/IUserRole";
import {IAccessRights, IGrantedAccess} from "../../access/IAccessRights";


export class ScorocodeAccessRights implements IAccessRights {


    grantUser(userID: string, operations: string[]) {

    }
    revokeUser(userID: string, operations: string[]) {

    }
    grantRole(roleID: string, operations: string[]) {

    }
    revokeRole(roleID: string, operations: string[]) {

    }

    grantAll(operations: string[]) {

    }
    revokeAll(operations: string[]) {

    }

    checkUserAccess(user: IUser, operations: string[]): boolean {
        return true;
    }

    get granted(): IGrantedAccess[] {
        return [];
    }
    get allGranted(): IGrantedAccess[] {
        return [];
    }


    public serialize(): Object {
        return {};
    }

    public static parse(data: Object): ScorocodeAccessRights {
        var rights = new ScorocodeAccessRights();
        return rights;
    }
}