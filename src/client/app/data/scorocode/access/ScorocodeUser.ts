import {IUser} from "../../access/IUser";
import {convertFromServer, convertToServer, ServerModelField} from "../../../utils/ServerModelFieldDecorator";

export class ScorocodeUserInfo {
    @ServerModelField("info")
    mInfo: string;
}

export class BaseScorocodeUser {
    @ServerModelField("email")
    mEmail: string;
}

export class ScorocodeUser extends BaseScorocodeUser implements IUser {
    @ServerModelField("username")
    mUserName: string;

    @ServerModelField("password")
    mPassword: string;
    
    @ServerModelField("doc")
    mDoc: ScorocodeUserInfo;

    public get displayName(): string {
        return this.mUserName;
    }
    public set displayName(val: string) {
        this.mUserName = val;
    }

    
    public get ID(): string {
        return this.mEmail;
    }

    public set ID(val: string) {
        this.mEmail = val;
    }

    public serialize(): any {
        return convertToServer(this);
    }

    public static parse(data: any): ScorocodeUser {
        return convertFromServer(data, ScorocodeUser);
    }
}