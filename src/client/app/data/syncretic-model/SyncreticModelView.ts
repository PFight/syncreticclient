export enum SyncreticModelView {
    ImageMap
}

export interface ISyncreticModelView {
    type: SyncreticModelView;
}

export interface ISyncreticModelElementView {
    type: SyncreticModelView;
}