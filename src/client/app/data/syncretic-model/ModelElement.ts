import { IAccessRights } from "../access/IAccessRights";
import {SyncreticModel, SyncreticModelTimestamp} from "./SyncreticModel";
import { ISyncreticModelElementView } from "./SyncreticModelView";
import {ITimestampObject} from "../ITimestampObject";

export class ModelElement {
    public name: string;
    public description: string;
    public ID: string;
    public access: IAccessRights;
    public parent: ModelElement;
    public children: ModelElement[];
    public parentModel: SyncreticModel;
    public view: ISyncreticModelElementView;

    public timestamp: ModelElementTimestamp;
    public userSpecific: ModelElementUserSpecific;
}

export class ModelElementTimestamp implements ITimestampObject {
    public targetID: string;
    public parent: ModelElementTimestamp;
    public children: ModelElementTimestamp[];

    /** Timestamp of the associated objects */
    public objectsTimestamp: number;
}

export class ModelElementUserSpecific {
    public targetID: string;
    public visible: boolean;
    public children: ModelElementUserSpecific[];
}