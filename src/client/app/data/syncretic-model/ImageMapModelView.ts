import {ISyncreticModelView, ISyncreticModelElementView, SyncreticModelView } "./SyncreticModelView"; 

export class ImageMapModelView: ISyncreticModelView {
    type: SyncreticModelView = SyncreticModelView.ImageMap;
    
    imageSrc: string;
    width: string;
    height: string;
    viewportWidth: string;
    viewportHeight: string;
}

export interface ISyncreticModelElementView {
    type: SyncreticModelView = SyncreticModelView.ImageMap;

    shape: string;
    coords: string;
}