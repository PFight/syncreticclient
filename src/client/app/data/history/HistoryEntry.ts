import {IDataEntity, EntityType } from "../IDataEntity";

export class HistoryEntry implements IDataEntity {
    public ID: string;
    public get type() { return EntityType.EntityComment }

    userID: string;
    dateTime: Date;
    targetID: string;
    targetType: HistoryEntryTragetType;
    displayValue: string;
}

export enum HistoryEntryTragetType {
    ModelCategoryTree,
    SyncreticModel,
    SyncreticObject
}