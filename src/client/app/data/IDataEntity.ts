import { IAccessRights } from "./access/IAccessRights";
import {ITimestampObject} from "./ITimestampObject";

/** Enums entities, that stared in database */
export enum EntityType {
    /** Tree of categories, without models */
    ModelCategoryTree,
    /** SyncreticModel with all its elements */
    SyncreticModel,
    /** SyncreticObject list */
    SyncreticObject,
    /** SyncreticObjectDetails of the specified SyncreticObject */
    SyncreticObjectDetails,
    /** Some of ITimestampObject dependent on target type */
    TimestampObject,
    /** History of CategoryTree, SyncreticModel */
    HistoryEntry,
    /** Comments of CategoryTree, SyncreticModel */
    EntityComment,
    /** Physical location of object - IObjectsSource */    
    ObjectSource
}

export interface IDataEntity {
    ID: string;
    type: EntityType;
    access?: IAccessRights;
    timestamp?: ITimestampObject;
}

export interface IUserSpecific {
    targetID: string;
    userID: string;
    visible: boolean;
}