import { IAccessRights } from "../access/IAccessRights";
import {ISyncreticObjectLocation} from "./ISyncreticObjectLocation";
import {CustomAttribute} from "./CustomAttribute";
import {HistoryEntry} from "../history/HistoryEntry";
import {EntityComment} from "../EntityComment";
import {IDataEntity, EntityType, IUserSpecific} from "../IDataEntity";

export class SyncreticObject implements IDataEntity {
    public ID: string;
    public name: string;
    public get type() { return EntityType.SyncreticObject }
    public objectType: string;
    public modelElements: ModelElementRef[]; 
    public customDigestAttributes: CustomAttribute[];
    public details: SyncreticObjectDetails;
}

export class ModelElementRef {
    /** Storing modelID to optimise search of element by ID */
    public modelID: string;
    public elementID: string;
}

export class SyncreticObjectDetails implements IDataEntity  {
    /** Parent SyncreticObject ID */
    public ID: string;
    public get type() { return EntityType.SyncreticObjectDetails }

    public description: string;
    public customAttributes: CustomAttribute[];
    public history: HistoryEntry[];
    public comments: EntityComment[];
    public access: IAccessRights;
    /** Info where object physically located */
    public location: ISyncreticObjectLocation;
}