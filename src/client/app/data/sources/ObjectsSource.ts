import {IDataEntity, EntityType } from "../IDataEntity";

abstract class ObjectsSource implements IDataEntity {
    public ID: string;
    public get type(): EntityType { return EntityType.ObjectSource }
    public access?: IAccessRights;
    public name: string;
    public sourceType: ObjectSourceType;
}

export ObjectSourceType {
    FileSystem
}