import {IDataEntity, EntityType } from "./IDataEntity";

export class EntityComment implements IDataEntity {
    public ID: string;
    public get type() { return EntityType.EntityComment }

    public userID: string;
    public text: string;
    public dateTime: Date; 
}